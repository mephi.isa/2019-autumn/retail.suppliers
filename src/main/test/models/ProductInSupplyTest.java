package models;

import org.junit.Assert;
import org.junit.Test;

public class ProductInSupplyTest {

    private String name = "milk";
    private String idAnalytics = "001";
    private String article = "1234";
    private String cond = "5-10 degree; 50% wet";
    private Product product = new Product(name,idAnalytics,article,cond);
    private Integer amount = 200;

    @Test
    public void TestPositive_ProductInSupplyConstructor() {
        ProductInSupply productInSupply = new ProductInSupply(product,amount);

        Assert.assertTrue(productInSupply.getProduct().equals(product) && productInSupply.getAmount().equals(amount));
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ProductInSupplyConstructorNullArgument() {
        ProductInSupply productInSupply = new ProductInSupply(null,amount);
    }

}
