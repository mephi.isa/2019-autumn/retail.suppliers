package models;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SupplyTest {

    private String name = "milk";
    private String idAnalytics = "001";
    private String article = "1234";
    private String cond = "5-10 degree; 50% wet";
    private Product product = new Product(name,idAnalytics,article,cond);
    private Integer amount = 200;
    private List<ProductInSupply> products = new ArrayList<ProductInSupply>();
    private String dueDate = "15.05.2019";
    private Date duDate = new SimpleDateFormat("dd.MM.yyyy").parse(dueDate);

    public SupplyTest() throws ParseException {
    }


    @Test
    public void TestPositive_SupplyConstructor() {
        ProductInSupply prod = new ProductInSupply(product,amount);
        products.add(prod);
        Supply sup = new Supply(duDate,products);
        Assert.assertTrue(sup.getCurrentDate().equals(duDate) && sup.getProducts().equals(products));
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_SupplyConstructorNullArgument() {
        Supply sup = new Supply(duDate,null);
    }

}



