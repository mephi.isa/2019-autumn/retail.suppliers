package models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContractTest {

    private Integer id = 1111;
    private Integer idEmployee = 1110;
    private Product prod = new Product("milk", "11", "111", "111");
    private Integer contractNum = 1;
    private ProductPull productPull = new ProductPull(prod, 10, 20.20);
    private List<ProductPull> products = new ArrayList<ProductPull>();
    private String provider = "Company";
    private String startDate = "01.11.2019";
    private String dueDate = "17.11.2019";
    private String dateOfSigning = "10.10.2019";
    private String dayOfSupply = "Fri";
    private String startPeriod = "01.11.2019";
    private String endPeriod = "30.11.2019";
    private String dateSignAgr1 = "05.11.2019";
    private String dateSignAgr2 = "10.11.2019";
    private String dueDateAgr2 = "29.11.2019";
    private ProductInSA productInSA1 = new ProductInSA(prod, 15, 20.20);
    private ProductInSA productInSA2 = new ProductInSA(prod, 30, 20.20);
    private ProductInSA productInSA3 = new ProductInSA(prod, 21, 20.20);
    private List<SupplementaryAgreement> agreements = new ArrayList<SupplementaryAgreement>();
    private Date stDate = new SimpleDateFormat("dd.MM.yyyy").parse(startDate);
    private Date duDate = new SimpleDateFormat("dd.MM.yyyy").parse(dueDate);
    private Date signDate = new SimpleDateFormat("dd.MM.yyyy").parse(dateOfSigning);
    private Date daySuppl = new SimpleDateFormat("EEEE").parse(dayOfSupply);
    //private SupplementaryAgreement agr1 = new SupplementaryAgreement();

    public ContractTest() throws ParseException {
    }

    @Test
    public void TestPositive_ContractConstructorWithManyArgs() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);

        Contract contract = new Contract(contractNum, idEmployee, products, provider, startDate, dueDate, dayOfSupply, proposal);

        Assert.assertTrue(contract.getProvider().equals(provider) && contract.getContractNum().equals(contractNum) &&
                contract.getDayOfSupply().equals(daySuppl) && contract.getDueDate().equals(duDate) &&
                contract.getStartDate().equals(stDate) && contract.getProducts().equals(products) &&
                contract.getProposal().equals(proposal) && contract.getIdEmloyee().equals(idEmployee) &&
                !contract.getStatus().isRejected() && !contract.getStatus().isApproved());
    }

    @Test
    public void TestPositive_ContractConstructorWithProposal() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);

        Contract contract = new Contract(contractNum, idEmployee, dayOfSupply, proposal);

        Assert.assertTrue(contract.getProvider().equals(provider) && contract.getContractNum().equals(contractNum) &&
                contract.getDayOfSupply().equals(daySuppl) && contract.getDueDate().equals(duDate) &&
                contract.getStartDate().equals(stDate) && contract.getProducts().equals(products) &&
                contract.getProposal().equals(proposal) && contract.getIdEmloyee().equals(idEmployee) &&
                !contract.getStatus().isRejected() && !contract.getStatus().isApproved());
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ContractNullArgument() {
        Contract contract = new Contract(contractNum, idEmployee, null, provider, startDate, dueDate, dayOfSupply, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ContractEmptyArgument() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);
        Contract contract = new Contract(contractNum, idEmployee, "", proposal);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ContractWrongDate() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, dueDate, startDate);
        Contract contract = new Contract(contractNum, idEmployee, products, provider, dueDate, startDate, dayOfSupply, proposal);
    }

    @Test
    public void TestPositive_ContractApprove() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);
        Contract contract = new Contract(contractNum, idEmployee, dayOfSupply, proposal);
        contract.approve(dateOfSigning);

        Assert.assertTrue(contract.getDateOfSigning().equals(signDate) && contract.getStatus().isApproved() &&
                !contract.getStatus().isRejected());
    }

    @Test
    public void TestPositive_ContractReject() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);
        Contract contract = new Contract(contractNum, idEmployee, dayOfSupply, proposal);
        contract.reject(dateOfSigning);

        Assert.assertTrue(contract.getDateOfSigning().equals(signDate) && !contract.getStatus().isApproved() &&
                contract.getStatus().isRejected());
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ContractReject() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);
        Contract contract = new Contract(contractNum, idEmployee, dayOfSupply, proposal);
        contract.reject(dueDateAgr2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ContractApprove() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);
        Contract contract = new Contract(contractNum, idEmployee, dayOfSupply, proposal);
        contract.approve(dueDateAgr2);
    }

    @Test
    public void TestPositive_SupplyList() throws ParseException {
        products.add(productPull);
      //  products.add(productPull);
      //  products.add(new ProductPull(new Product("coca-cola","11","11111","30"),20,20.20));
        CommericalProposal proposal = new CommericalProposal(id, idEmployee, products, provider, startDate, dueDate);
        Contract contract = new Contract(contractNum, idEmployee, dayOfSupply, proposal);
        contract.approve(dateOfSigning);
        List<ProductInSA> productInSAs1 = new ArrayList<ProductInSA>();
        productInSAs1.add(productInSA1);
        SupplementaryAgreement agreement = new SupplementaryAgreement(1,dueDate,productInSAs1);
        contract.addAgreement(agreement);
        List<ProductInSA> productInSAs2 = new ArrayList<ProductInSA>();
        productInSAs2.add(productInSA2);
        SupplementaryAgreement agreement1 = new SupplementaryAgreement(2,dueDateAgr2,productInSAs2);
        contract.addAgreement(agreement1);
        List<ProductInSA> productInSAs3 = new ArrayList<ProductInSA>();
        productInSAs3.add(productInSA3);
        SupplementaryAgreement agreement2 = new SupplementaryAgreement(3,dueDateAgr2,productInSAs3);
        contract.addAgreement(agreement2);
    //    System.out.println("Amount of supply: "+contract.howManySupply(stDate,duDate,daySuppl)+"\n");
     //   System.out.println("Amount of milk: "+contract.countAllAmount(duDate,productPull.getProduct(),true));
        List<Supply> res = contract.createPlanOfSupply(startPeriod,endPeriod);
  //      for (int i=0; i<res.size();i++) {
  //          res.get(i).Print();
  //      }
        // contract.Print();
        String date1 = "01.11.2019";
        String date2 = "08.11.2019";
        String date3 = "15.11.2019";
        String date4 = "22.11.2019";
        String date5 = "29.11.2019";
        Date dat1 = new SimpleDateFormat("dd.MM.yyyy").parse(date1);
        Date dat2 = new SimpleDateFormat("dd.MM.yyyy").parse(date2);
        Date dat3 = new SimpleDateFormat("dd.MM.yyyy").parse(date3);
        Date dat4 = new SimpleDateFormat("dd.MM.yyyy").parse(date4);
        Date dat5 = new SimpleDateFormat("dd.MM.yyyy").parse(date5);
        List<Date> dats = new ArrayList<Date>();
        dats.add(dat1);
        dats.add(dat2);
        dats.add(dat3);
        dats.add(dat4);
        dats.add(dat5);
        List<Integer> values = new ArrayList<Integer>();
        values.add(9);
        values.add(8);
        values.add(8);
        values.add(26);
        values.add(25);

        for (int i=0; i<res.size();i++) {
            Supply elem = res.get(i);
            Assert.assertEquals(elem.getCurrentDate(),dats.get(i));
            int actual = elem.getProducts().get(0).getAmount();
            int expec = values.get(i);
            Assert.assertEquals(actual,expec);
        }
    }

}