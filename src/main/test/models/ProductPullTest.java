package models;

import org.junit.Assert;
import org.junit.Test;

public class ProductPullTest {

    private String name = "milk";
    private String idAnalytics = "001";
    private String article = "1234";
    private String cond = "5-10 degree; 50% wet";
    private Product product = new Product(name,idAnalytics,article,cond);
    private Integer amount = 200;
    private Double price = 20.20;
    @Test
    public void TestPositive_ProductPullConstructor() {
        ProductPull productPull = new ProductPull(product,amount,price);

        Assert.assertTrue(productPull.getProduct().equals(product) && productPull.getAmount().equals(amount) &&
                            productPull.getPrice().equals(price));
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ProductPullConstructorNullArgument() {
        ProductPull productPull = new ProductPull(null,amount,price);
    }

}



