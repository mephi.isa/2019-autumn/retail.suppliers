package models;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SupplementaryAgreementTest {

    private String name = "milk";
    private Integer idSA = 12;
    private String idAnalytics = "001";
    private String article = "1234";
    private String cond = "5-10 degree; 50% wet";
    private Product product = new Product(name,idAnalytics,article,cond);
    private Integer amount = 200;
    private Double price = 20.20;
    private String dueDate = "15.05.2019";
    private Date duDate = new SimpleDateFormat("dd.MM.yyyy").parse(dueDate);
    ProductInSA productInSA = new ProductInSA(product,amount,price);
    private String dateOfSigning = "10.04.2019";
    private Date dateOfSign = new SimpleDateFormat("dd.MM.yyyy").parse(dateOfSigning);

    public SupplementaryAgreementTest() throws ParseException {
    }

    @Test
    public void TestPositive_SAConstructor() {
        List<ProductInSA> products = new ArrayList<ProductInSA>();
        products.add(productInSA);
        SupplementaryAgreement SA = new SupplementaryAgreement(idSA,dueDate,products);

        Assert.assertTrue(SA.getIdSA().equals(idSA) && SA.getDueDate().equals(duDate) && SA.getProducts().equals(products) && !SA.getStatus().isApproved()
                            && !SA.getStatus().isRejected());
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_SAConstructorNullArgument() {
        SupplementaryAgreement SA = new SupplementaryAgreement(idSA,dueDate,null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_SAConstructorEmptyArgument() {
        SupplementaryAgreement SA = new SupplementaryAgreement(idSA,"",null);
    }

    @Test
    public void TestPositive_SAApprove() {
        List<ProductInSA> products = new ArrayList<ProductInSA>();
        products.add(productInSA);
        SupplementaryAgreement SA = new SupplementaryAgreement(idSA,dueDate,products);

        SA.approve(dateOfSigning);

        Assert.assertTrue(SA.getDataOfSigning().equals(dateOfSign) && !SA.getStatus().isRejected() && SA.getStatus().isApproved());
    }

    @Test
    public void TestPositive_SAReject() {
        List<ProductInSA> products = new ArrayList<ProductInSA>();
        products.add(productInSA);
        SupplementaryAgreement SA = new SupplementaryAgreement(idSA,dueDate,products);

        SA.reject(dateOfSigning);

        Assert.assertTrue(SA.getDataOfSigning().equals(dateOfSign) && SA.getStatus().isRejected() && !SA.getStatus().isApproved());
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_SAApprove() {
        List<ProductInSA> products = new ArrayList<ProductInSA>();
        products.add(productInSA);
        SupplementaryAgreement SA = new SupplementaryAgreement(idSA,dateOfSigning,products);

        SA.approve(dueDate);

       // Assert.assertTrue(SA.getDataOfSigning().equals(duDate) && !SA.getStatus().isRejected() && SA.getStatus().isApproved());
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_SAReject() {
        List<ProductInSA> products = new ArrayList<ProductInSA>();
        products.add(productInSA);
        SupplementaryAgreement SA = new SupplementaryAgreement(idSA,dateOfSigning,products);

        SA.reject(dueDate);

        // Assert.assertTrue(SA.getDataOfSigning().equals(duDate) && !SA.getStatus().isRejected() && SA.getStatus().isApproved());
    }
}
