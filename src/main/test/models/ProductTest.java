package models;

import org.junit.Assert;
import org.junit.Test;

public class ProductTest {

    private String name = "milk";
    private String idAnalytics = "001";
    private String article = "1234";
    private String cond = "5-10 degree; 50% wet";

    @Test
    public void TestPositive_ProductConstructor() {
        Product product = new Product(name,idAnalytics,article,cond);

        Assert.assertTrue(product.getName().equals(name) && product.getIdAnalytics().equals(idAnalytics) &&
                                        product.getArticle().equals(article) && product.getConditions().equals(cond));
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ProductConstructorNullArgument() {
        Product product = new Product(name,idAnalytics,article,null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ProductConstructorEmptyArgument() {
        Product product = new Product(name,"",article, cond);
    }
}
