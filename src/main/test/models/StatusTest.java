package models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StatusTest {

    @Test
    public void TestPositive_StatusConstructor() {
        Status stat = new Status();

        Assert.assertTrue(!stat.isApproved() && !stat.isRejected());
    }

    @Test
    public void TestPositive_StatusApprove() {
        Status stat = new Status();
        stat.approve();

        Assert.assertTrue(!stat.isRejected() && stat.isApproved());
    }

    @Test
    public void TestPositive_StatusReject() {
        Status stat = new Status();
        stat.reject();

        Assert.assertTrue(stat.isRejected() && !stat.isApproved());
    }
}

