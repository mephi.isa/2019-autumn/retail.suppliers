package models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommericalProposalTest {

    private Integer id = 1111;
    private Integer idEmployee = 1110;
    private Product prod = new Product("milk","11","111","111");
    private ProductPull productPull = new ProductPull(prod,20,20.20);
    private List<ProductPull> products = new ArrayList<ProductPull>();
    private String provider = "Company";
    private String startDate = "11.04.2019";
    private String dueDate = "15.05.2019";
    private String dateOfSigning = "10.04.2019";

    private Date stDate = new SimpleDateFormat("dd.MM.yyyy").parse(startDate);
    private Date duDate = new SimpleDateFormat("dd.MM.yyyy").parse(dueDate);
    private Date signDate = new SimpleDateFormat("dd.MM.yyyy").parse(dateOfSigning);

    public CommericalProposalTest() throws ParseException {
    }

    @Test
    public void TestPositive_CommericalProposalConstructor() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,provider,startDate,dueDate);
        Assert.assertTrue(proposal.getId().equals(id) && !proposal.getStatus().isRejected() &&
                proposal.getIdEmployee().equals(idEmployee) && proposal.getProducts().equals(products) &&
                proposal.getProvider().equals(provider) && proposal.getStartDate().equals(stDate) &&
                proposal.getDueDate().equals(duDate) && !proposal.getStatus().isApproved() );
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_CommericalProposalNullArgument() {
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,null,provider,startDate,dueDate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_CommericalProposalEmptyArgument() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,"",startDate,dueDate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_CommericalProposalWrongDate() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,provider,dueDate,startDate);
    }

    @Test
    public void TestPositive_CommericalProposalApprove() throws Exception {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,provider,startDate,dueDate);
        proposal.approve(dateOfSigning);

        Assert.assertTrue(proposal.getDateOfSigning().equals(signDate) && proposal.getStatus().isApproved() &&
                            !proposal.getStatus().isRejected());
    }

    @Test
    public void TestPositive_CommericalProposalReject() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,provider,startDate,dueDate);
        proposal.reject(dateOfSigning);

        Assert.assertTrue(proposal.getDateOfSigning().equals(signDate) && !proposal.getStatus().isApproved() &&
                proposal.getStatus().isRejected());
    }

    @Test (expected = IllegalArgumentException.class)
    public void TestNegative_CommericalProposalReject() {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,provider,startDate,dateOfSigning);
        proposal.reject(dueDate);
    }

    @Test (expected = IllegalArgumentException.class)
    public void TestNegative_CommericalProposalApprove() throws Exception {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,provider,startDate,dateOfSigning);
        proposal.approve(dueDate);
    }

    @Test (expected = Exception.class)
    public void TestNegative_CommericalProposalApproveRejectedCP() throws Exception {
        products.add(productPull);
        CommericalProposal proposal = new CommericalProposal(id,idEmployee,products,provider,startDate,dateOfSigning);
        proposal.reject(dueDate);
        proposal.approve(dueDate);
    }
}
