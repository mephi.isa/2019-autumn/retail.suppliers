package models;

import org.junit.Assert;
import org.junit.Test;

public class ProductInSATest {

    private String name = "milk";
    private String idAnalytics = "001";
    private String article = "1234";
    private String cond = "5-10 degree; 50% wet";
    private Product product = new Product(name,idAnalytics,article,cond);
    private Integer amount = 200;
    private Double price = 20.20;
    @Test
    public void TestPositive_ProductInSAConstructor() {
        ProductInSA productInSA = new ProductInSA(product,amount,price);

        Assert.assertTrue(productInSA.getProduct().equals(product) && productInSA.getAmount().equals(amount) &&
                productInSA.getPrice().equals(price));
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestNegative_ProductInSAConstructorNullArgument() {
        ProductInSA productInSA = new ProductInSA(null,amount,price);
    }

}
