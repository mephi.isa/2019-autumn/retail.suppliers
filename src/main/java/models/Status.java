package models;

public class Status {
    private boolean approved;
    private boolean rejected;

    public Status() {
        this.approved = false;
        this.rejected = false;
    }

    public boolean isApproved() {
        return approved;
    }

    public void approve() {
        this.approved = true;
        this.rejected = false;
    }

    public boolean isRejected() {
        return rejected;
    }

    public void reject() {
        this.rejected = true;
        this.approved = false;
    }
}
