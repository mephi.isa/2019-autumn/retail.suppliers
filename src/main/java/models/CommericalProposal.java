package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommericalProposal {

    private Integer id;
    private Integer idEmployee;
    private List<ProductPull> products;
    private String provider;
    private Date startDate;
    private Date dueDate;
    private Date dateOfSigning;
    private Status status;



    public CommericalProposal(Integer id, Integer idEmployee, List<ProductPull> products, String provider,
                              String startDate, String dueDate) {
        if (id != null && !"".equals(id) && idEmployee != null && !"".equals(idEmployee) &&
                products != null && !"".equals(provider) && provider != null && !"".equals(startDate)
                && startDate != null && !"".equals(dueDate) && dueDate !=null) {
            this.id = id;
            this.idEmployee = idEmployee;
            this.products = products;
            this.provider = provider;
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            try {
                this.startDate = format.parse(startDate);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            try {
                this.dueDate = format.parse(dueDate);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            if (this.startDate.after(this.dueDate)) {
                throw new IllegalArgumentException("Due date must be after start");
            }
            this.status = new Status();
        } else {
            throw new IllegalArgumentException("Commerical proposal constructor arguments cant be NULL");
        }
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public List<ProductPull> getProducts() {
        return products;
    }

    public String getProvider() {
        return provider;
    }


    public Date getStartDate() {
        return startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Date getDateOfSigning() {
        return dateOfSigning;
    }

    public Status getStatus() {
        return status;
    }

    public void approve(String dateOfSigning) throws Exception {
        if (this.status.isRejected() ) {
            throw new Exception("this agreement is rejected");
        } else {
            this.status.approve();
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            try {
                if (this.startDate.before(format.parse(dateOfSigning))) {
                    throw new IllegalArgumentException("Date of signing must be before start");
                }
                this.dateOfSigning = format.parse(dateOfSigning);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }

    public void reject(String dateOfRejecting) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
            if (this.startDate.before(format.parse(dateOfRejecting))) {
                throw new IllegalArgumentException("Date of rejecting must be before start");
            }
            this.dateOfSigning = format.parse(dateOfRejecting);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        this.status.reject();
    }

}
