package models;

public class ProductPull {

    private Product product;
    private Integer amount;
    private Double price;

    public ProductPull(Product product, Integer amount, Double price) {
        if (product != null && amount != null && price != null) {
            this.product = product;
            this.amount = amount;
            this.price = price;
        } else {
            throw new IllegalArgumentException("ProductPull constructor arguments cant be NULL");
        }
    }

    public void reduceAmount(Integer amount) {
        this.amount -= amount;
    }

    public void raiseAmount(Integer amount) {
        this.amount += amount;
    }

    public Product getProduct() {
        return product;
    }

    public Integer getAmount() {
        return amount;
    }

    public Double getPrice() {
        return price;
    }


}

