package models;


import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.ToDoubleBiFunction;

public class Contract {

    private Integer contractNum;
    private Integer idEmloyee;
    private List<ProductPull> products;
    private String provider;
    private Date startDate;
    private Date dueDate;
    private Date dayOfSupply;
    private Date dateOfSigning;
    private Status status;
    private List<SupplementaryAgreement> agreements;
    private CommericalProposal proposal;

    public Contract(Integer contractNum, Integer idEmloyee, List<ProductPull> products,
                        String provider, String startDate, String dueDate, String dayOfSupply,
                        CommericalProposal proposal) {
        if (contractNum != null && idEmloyee != null && products != null && provider != null && !"".equals(provider) &&
               startDate != null && !"".equals(startDate) && dueDate != null && !"".equals(dueDate) &&
                dayOfSupply != null && !"".equals(dayOfSupply) && proposal != null) {
            this.contractNum = contractNum;
            this.idEmloyee = idEmloyee;
            this.products = products;
            this.provider = provider;
            this.proposal = proposal;
            this.status = new Status();
            this.agreements = new ArrayList<SupplementaryAgreement>();
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            try {
                this.startDate = format.parse(startDate);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            try {
                this.dueDate = format.parse(dueDate);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            format = new SimpleDateFormat("EEEE");
            try {
                this.dayOfSupply = format.parse(dayOfSupply);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            throw new IllegalArgumentException("Contract need not null args");
        }
    }

    public Contract(Integer contractNum, Integer idEmloyee, String dayOfSupply, CommericalProposal proposal) {
        if (contractNum != null && idEmloyee != null && dayOfSupply != null && !"".equals(dayOfSupply) && proposal != null) {
            this.contractNum = contractNum;
            this.idEmloyee = idEmloyee;
            this.products = proposal.getProducts();
            this.provider = proposal.getProvider();
            this.startDate = proposal.getStartDate();
            this.dueDate = proposal.getDueDate();
            this.agreements = new ArrayList<SupplementaryAgreement>();
            SimpleDateFormat format = new SimpleDateFormat("EEEE");
            try {
                this.dayOfSupply = format.parse(dayOfSupply);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            this.proposal = proposal;
            this.status = new Status();
        } else {
            throw new IllegalArgumentException("Conract need not null args");
        }
    }

    public Integer getContractNum() {
        return contractNum;
    }

    public Integer getIdEmloyee() {
        return idEmloyee;
    }

    public List<ProductPull> getProducts() {
        return products;
    }

    public String getProvider() {
        return provider;
    }

    public List<SupplementaryAgreement> getAgreements() {
        return agreements;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Date getDayOfSupply() {
        return dayOfSupply;
    }

    public Date getDateOfSigning() {
        return dateOfSigning;
    }

    public Status getStatus() {
        return status;
    }

    public CommericalProposal getProposal() {
        return proposal;
    }

    public void approve (String dateOfSigning) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
            this.dateOfSigning = format.parse(dateOfSigning);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        this.status.approve();
        if (this.startDate.before(this.dateOfSigning)) {
            throw new IllegalArgumentException("Date of rejecting must be before start");
        }
    }

    public void reject (String dateOfSigning) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
            this.dateOfSigning = format.parse(dateOfSigning);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        this.status.reject();
        if (this.startDate.before(this.dateOfSigning)) {
            throw new IllegalArgumentException("Date of rejecting must be before start");
        }
    }

    private int getDay (Date startDate) {
        int day = 0;
        String dayWeek = new SimpleDateFormat("EEEE").format(startDate).toString();
        switch (dayWeek) {
            case "Monday": day = 1;
                break;
            case "Tuesday": day = 2;
                break;
            case "Wednesday": day = 3;
                break;
            case "Thursday": day = 4;
                break;
            case "Friday": day = 5;
                break;
            case "Saturday": day = 6;
                break;
            case "Sunday": day = 7;
                break;
        }
        //      System.out.println(dayWeek + " which is " + day + " of week");
        return day;
    }

    private int diffInDays (Date startDate, Date endDate) {
        long diff = endDate.getTime() - startDate.getTime();
        int days = (int) (diff / (24*60*60*1000));
        return days;
    }

    private int howManySupply (Date startDate, Date endDate, Date dayOfSupply) {
        int dayOfS = this.getDay(dayOfSupply);
        int diffInDays = this.diffInDays(startDate,endDate);
        int howManySupply = diffInDays / 7;
        if (dayOfS <= this.getDay(endDate)) {
            if (this.getDay(startDate) <= dayOfS) {
                howManySupply ++;
            } else {
                if (dayOfS == this.getDay(endDate))
                    howManySupply++;
            }
        }
        return howManySupply;
    }

    private List<Date> checkAllDateAgreement (Date startDate, Date endDate) {
        List<Date> res = new ArrayList<Date>();
   //     System.out.println("here amount of agr: "+this.agreements.size());
        for (int i = 0; i < this.agreements.size(); i++) {
       //     System.out.println("here1");
            SupplementaryAgreement elem = this.agreements.get(i);
            boolean n=false;
            if (res.isEmpty()) {
            //    System.out.println("here");
                res.add(elem.getDueDate());
            //    System.out.println("here date; "+elem.getDueDate());
            } else {
                for (int j = 0; j < res.size(); j++) {
                    if (res.contains(elem.getDueDate())) {
                     //   System.out.println("fuck");
                        n = false;
                    } else {
                        if (startDate.before(elem.getDueDate()) && endDate.after(elem.getDueDate())) {
                            //res.add(elem.getDueDate());
                            n = true;
                        }
                    }
                }
                if (n) {
                    res.add(elem.getDueDate());
                }
            }
        }
        return res;
    }

    private int countAllAmount (Date dueDate, Product product, boolean needNonAgree) {
        int res = 0;
        if (needNonAgree) {
            for (int i = 0; i < this.products.size(); i++) {
                if (this.products.get(i).getProduct() == product) {
                    res += this.products.get(i).getAmount();
                }
            }
        }
        List<SupplementaryAgreement> agr = checkNonLongAgreement(dueDate);
        if (!agr.isEmpty()) {
            for (int i =0 ; i < agr.size(); i++) {
                for (int j = 0; j < agr.get(i).getProducts().size(); j++) {
                    if (!agr.get(i).getDueDate().before(dueDate)) {
                        if (product == agr.get(i).getProducts().get(j).getProduct())
                            res += agr.get(i).getProducts().get(j).getAmount();
                    }
                }
            }
        }
        return res;
    }

    private List<SupplementaryAgreement> checkNonLongAgreement (Date dueDate) {
        List<SupplementaryAgreement> agr = new ArrayList<SupplementaryAgreement>();
        if (!this.agreements.isEmpty()) {
            for (int i = 0; i < this.agreements.size(); i++) {
                SupplementaryAgreement elem = this.agreements.get(i);
                if (!elem.getDueDate().after(dueDate)) {
                    agr.add(elem);
                }
            }
        }
        return agr;
    }


    private List<Supply> createList (Date dueDate, Date startDate, boolean needNonAgreement) {
        int Nd = howManySupply(startDate,dueDate,this.dayOfSupply);
        if (Nd == 0) {
            return null;
        }
        LocalDate ld = new java.sql.Date(startDate.getTime()).toLocalDate();
        LocalDate dayWeek = new java.sql.Date(this.dayOfSupply.getTime()).toLocalDate();
        LocalDate firstSupDay = null;
        if (getDay(startDate) >= getDay(this.dayOfSupply)) {
            firstSupDay = (new java.sql.Date(startDate.getTime()).toLocalDate()).with(TemporalAdjusters.nextOrSame(dayWeek.getDayOfWeek()));
        } else {
            firstSupDay = (new java.sql.Date(startDate.getTime()).toLocalDate()).with(TemporalAdjusters.previous(dayWeek.getDayOfWeek()));
        }
        List<Supply> res = new ArrayList<Supply>();
        if (needNonAgreement) {
            for (int j = 0; j < Nd; j++) {
                List<ProductInSupply> productList = new ArrayList<ProductInSupply>();
                for (int i = 0; i < this.products.size(); i++) {
                    ProductPull elem = this.products.get(i);
                    int Np = countAllAmount(dueDate, elem.getProduct(),true);
                    int N = 0;
                    if ((j) < (Np % Nd)) {
                        if ((Np % Nd) != 0) {
                            N = (Np / Nd) + 1;
                        } else {
                            N = (Np/Nd);
                        }
                    } else {
                        N = (Np/Nd);
                    }
                    ProductInSupply prod = new ProductInSupply(elem.getProduct(), N);
                    productList.add(prod);
                }
                Supply sup = null;
                if (j == 0) {
                    Date date = Date.from(firstSupDay.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    sup = new Supply(date, productList);
                    firstSupDay = firstSupDay.with(TemporalAdjusters.next(dayWeek.getDayOfWeek()));
                } else {
                    Date date = Date.from(firstSupDay.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    sup = new Supply(date, productList);
                    firstSupDay = firstSupDay.with(TemporalAdjusters.next(dayWeek.getDayOfWeek()));
                }
                res.add(sup);
            }
        } else {
            for (int j = 0; j < Nd; j++) {
                List<ProductInSupply> productList = new ArrayList<ProductInSupply>();
                for (int i = 0; i < this.products.size(); i++) {
                    ProductPull elem = this.products.get(i);
                    int Np = countAllAmount(dueDate, elem.getProduct(), false);
                    int N = 0;
                    if ((j) < (Np % Nd)) {
                        if ((Np % Nd) != 0) {
                            N = (Np / Nd) + 1;
                        } else {
                            N = (Np/Nd);
                        }
                    } else {
                        N = (Np/Nd);
                    }
                    ProductInSupply prod = new ProductInSupply(elem.getProduct(), N);
                    productList.add(prod);
                }
                Supply sup = null;
                if (j == 0) {
                    Date date = Date.from(firstSupDay.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    sup = new Supply(date, productList);
                } else {
                    firstSupDay = firstSupDay.with(TemporalAdjusters.next(dayWeek.getDayOfWeek()));
                    Date date = Date.from(firstSupDay.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    sup = new Supply(date, productList);
                }
                res.add(sup);
            }
        }
        return res;
    }

    //TODO
    public List<Supply> createPlanOfSupply (String startOfPeriod, String endOfPeriod) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = format.parse(startOfPeriod);
            endDate = format.parse(endOfPeriod);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }

        List<Supply> res1 = new ArrayList<Supply>();

        List<Date> dates = checkAllDateAgreement(startDate, endDate);
        if (dates.isEmpty()) {
            if (endDate.after(this.dueDate)) {
                endDate = this.dueDate;
            }
            res1 = createList(endDate,startDate,true);
        } else {
            for (int i = 0; i < dates.size(); i++) {
                if (dates.get(i).equals(this.dueDate)) {
                    List<Supply> res = createList(dates.get(i), startDate, true);
                    startDate = dates.get(i);
                    if (res != null) {
                        res1.addAll(res);
                    }
                } else {
                    List<Supply> res = createList(dates.get(i), startDate, false);
                    startDate = dates.get(i);
                    if (res != null) {
                        res1.addAll(res);
                    }
                }

            }
        }
        return res1;
    }


//    public void addAgreement (Date dueDate, List<ProductPull> products) {
//        if (this.agreements == null) {
//            this.agreements = new ArrayList<SupplementaryAgreement>();
//        }
//        this.agreements.add(new SupplementaryAgreement(dueDate,products));
//    }

    public void addAgreement (SupplementaryAgreement newAgreement) {
        if (this.agreements == null) {
            this.agreements = new ArrayList<SupplementaryAgreement>();
        }
        this.agreements.add(newAgreement);
    }



}
