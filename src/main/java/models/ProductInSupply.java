package models;

public class ProductInSupply {
    private Product product;
    private Integer amount;

    public ProductInSupply(Product product, Integer amount) {
        if (product != null && amount != null ) {
            this.product = product;
            this.amount = amount;
        } else {
            throw new IllegalArgumentException("ProductInSupply constructor arguments cant be NULL");
        }
    }

    public Product getProduct() {
        return product;
    }

    public Integer getAmount() {
        return amount;
    }

}
