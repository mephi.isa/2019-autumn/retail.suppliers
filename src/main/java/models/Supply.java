package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Supply {
    private Date currentDate;
    private List<ProductInSupply> products;

    public Supply(Date dueDate, List<ProductInSupply> products) {
    //    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
    //    try {
    //        this.currentDate = format.parse(dueDate);
    //    } catch (Exception e) {
    //           System.out.println(e.getMessage());
    //    }
        if (dueDate != null && products != null) {
            this.currentDate = dueDate;
            this.products = products;
        } else {
            throw new IllegalArgumentException("Supply need non null args");
        }
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public List<ProductInSupply> getProducts() {
        return products;
    }


}
