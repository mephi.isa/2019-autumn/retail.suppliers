package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SupplementaryAgreement {
    private Integer idSA;
    private Date dueDate;
    private Date dataOfSigning;
    private List<ProductInSA> products;
    private Status status;

    public SupplementaryAgreement(Integer idSA, String dueDate, List<ProductInSA> products) {
        if (idSA != null && !"".equals(dueDate) && dueDate != null && products != null) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            try {
                this.dueDate = format.parse(dueDate);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            this.idSA = idSA;
            this.products = products;
            this.status = new Status();
        } else {
            throw new IllegalArgumentException("SupplementaryAgreement need not null args");
        }
    }

    public void approve (String dataOfSigning) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
               this.dataOfSigning = format.parse(dataOfSigning);
        } catch (Exception e) {
                System.out.println(e.getMessage());
        }
        this.status.approve();
        if (this.dataOfSigning.after(dueDate))
            throw new IllegalArgumentException("wrong date of Signing");
    }

    public void reject (String dataOfSigning) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
            this.dataOfSigning = format.parse(dataOfSigning);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        this.status.reject();
        if (this.dataOfSigning.after(dueDate))
            throw new IllegalArgumentException("wrong date of rejcting");
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Integer getIdSA() {
        return idSA;
    }

    public Date getDataOfSigning() {
        return dataOfSigning;
    }

    public List<ProductInSA> getProducts() {
        return products;
    }

    public Status getStatus() {
        return status;
    }
}
