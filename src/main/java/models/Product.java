package models;

public class Product {

    private String name;
    private String idAnalytics;
    private String article;
    private String description;
    private String conditions;

    public Product(String name, String idAnalytics, String article, String conditions) {
        if (name != null && !"".equals(name) && idAnalytics != null && !"".equals(idAnalytics) &&
                article != null && !"".equals(article) && conditions != null && !"".equals(conditions)) {
            this.name = name;
            this.idAnalytics = idAnalytics;
            this.article = article;
            this.conditions = conditions;
        } else {
            throw new IllegalArgumentException("Product constructor arguments cant be NULL");
        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getIdAnalytics() {
        return idAnalytics;
    }

    public String getArticle() {
        return article;
    }

    public String getDescription() {
        return description;
    }

    public String getConditions() {
        return conditions;
    }

    @Override
    public boolean equals(Object o){
        if (this==o) return true;
        if (o==null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return name.equals(product.name) && idAnalytics.equals(product.idAnalytics) && article.equals(product.article)
                && conditions.equals(product.conditions);
    }
}

