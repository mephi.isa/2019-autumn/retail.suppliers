# Модель бизнес-процессов

##### Содержание:    
  1. [Создание коммерческого предложения и отправление КП](#createKP)
  2. [Обновление КП](#changeKP)
  3. [Отклонение КП](#rejectKP)
  4. [Создание договора](#createContr)
  5. [Дополнительное соглашение к договору](#changeContr)
  6. [Расторжение договора](#rejContr)
  
  
 ## <a name="createKP"></a>Создание коммерческого предложения и отправление КП
   ![Создание коммерческого предложения и отправление КП](_static/commercial_offer_creation_and_sending.png)
   
 ## <a name="changeKP"></a>Обновление КП
   ![Обновление КП](_static/commercial_offer_updating.png)
 
 ## <a name="rejectKP"></a>Отклонение КП
   ![Отклонение КП](_static/commercial_offer_rejection.png)
  
 ## <a name="createContr"></a>Создание договора
   ![Создание договора](_static/сontract_creation.png)
   
 ## <a name="changeContr"></a>Дополнительное соглашение к договору
   ![Обновление договора](_static/сontract_updating.png)

 ## <a name="rejContr"/>Расторжение договора
   ![Отклонение договора](_static/сontract_rejection.png)
