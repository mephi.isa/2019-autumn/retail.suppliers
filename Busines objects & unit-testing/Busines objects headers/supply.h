#ifndef SUPPLY_H
#define SUPPLY_H

#include "productinsupply.h"
#include <QDate>
#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>

class Supply
{
private:
    QDate supplyDueDate;
    std::list<ProductInSupply> supplyProducts;

public:
    Supply();
    ~Supply();
    QDate getSupplyDueDate() const;
    void setSupplyDueDate(const QDate &value);
    std::list<ProductInSupply> getSupplyProducts() const;
    void setSupplyProducts(const std::list<ProductInSupply> &value);
};

#endif // SUPPLY_H
