#ifndef COMMERCIALPROPOSAL_H
#define COMMERCIALPROPOSAL_H


#include "productpull.h"
#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>
#include <QDate>

class CommercialProposal
{
private:
    int         proposalIndex;
    int         proposalEmployeeID;
    std::string proposalSupplier;
    QDate       proposalDueDate;
    QDate       proposalStartDate;
    QDate       proposalSignDate;
    std::string proposalStatus;
    std::list <ProductPull> proposalProducts;
public:
    CommercialProposal();
    ~CommercialProposal();

    int getProposalIndex() const;
    void setProposalIndex(int value);
    int getProposalEmployeeID() const;
    void setProposalEmployeeID(int value);
    std::string getProposalSupplier() const;
    void setProposalSupplier(const std::string &value);
    QDate getProposalDueDate() const;
    void setProposalDueDate(const QDate &value);
    QDate getProposalStartDate() const;
    void setProposalStartDate(const QDate &value);
    QDate getProposalSignDate() const;
    void setProposalSignDate(const QDate &value);
    std::string getProposalStatus() const;
    void setProposalStatus(const std::string &value);
    std::list<ProductPull> getProposalProducts() const;
    void setProposalProducts(const std::list<ProductPull> &value);

    std::string MakeProposal(int index, int empl_id, std::list<ProductPull> *product_list, std::string provider, QDate dd, QDate sd);
    void RejectCommercialProposal(QDate signDate);
    void ApproveCommercialProposal(QDate signDate);
};

#endif // COMMERCIALPROPOSAL_H
