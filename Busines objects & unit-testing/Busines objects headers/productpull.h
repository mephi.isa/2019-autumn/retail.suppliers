#ifndef PRODUCTPULL_H
#define PRODUCTPULL_H

#include "product.h"
#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>
#include <QDate>

class ProductPull
{
private:
    Product pullItem;
    int     pullItemAmount;
    double  pullItemPrice;


public:
    ProductPull();
    ~ProductPull();
    Product getPullItem() const;
    void setPullItem(const Product &value);
    int getPullItemAmount() const;
    void setPullItemAmount(int value);
    double getPullItemPrice() const;
    void setPullItemPrice(double value);
    void reduceProductPullAmount(int amount);
    void raiseProductPullAmount(int amount);
};



#endif // PRODUCTPULL_H
