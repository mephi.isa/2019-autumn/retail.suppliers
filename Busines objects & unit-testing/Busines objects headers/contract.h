#ifndef CONTRACT_H
#define CONTRACT_H

#include "supplementaryagreement.h"
#include "supply.h"
#include "commercialproposal.h"
#include "productpull.h"
#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>
#include <QDate>

class Contract
{
private:

    int         contractIndex;
    int         contractEmployeeID;
    std::string contractSupplier;
    QDate       contractStartDate;
    QDate       contractDueDate;
    QDate       contractSupplyDate;
    QDate       contractSignDate;
    std::string contractStatus;
    CommercialProposal contractProposal;
    std::list<SupplementaryAgreement> contractAgreements;
    std::list <ProductPull> contractProducts;


public:
    Contract(int index,QDate startDate, QDate dueDate,std::string status);
    ~Contract();

    int getContractIndex() const;
    void setContractIndex(int value);
    int getContractEmployeeID() const;
    void setContractEmployeeID(int value);
    std::string getContractSupplier() const;
    void setContractSupplier(const std::string &value);
    QDate getContractStartDate() const;
    void setContractStartDate(const QDate &value);
    QDate getContractDueDate() const;
    void setContractDueDate(const QDate &value);
    QDate getContractSupplyDate() const;
    void setContractSupplyDate(const QDate &value);
    QDate getContractSignDate() const;
    void setContractSignDate(const QDate &value);
    std::string getContractStatus() const;
    void setContractStatus(const std::string &value);
    std::list<ProductPull> getContractProducts() const;
    void setContractProducts(const std::list<ProductPull> &value);
    CommercialProposal getContractProposal() const;
    void setContractProposal(const CommercialProposal &value);
    std::list<SupplementaryAgreement> getContractAgreements() const;
    void setContractAgreements(const std::list<SupplementaryAgreement> &value);

    void AddSupplementaryAgreement(SupplementaryAgreement agreement);
    void RejectContract(QDate signDate);
    void ApproveContract(QDate signDate);
    bool ComposeContract(int index, int empl_id, QDate startDate, QDate supplyDate, std::list<ProductPull> *prod_list, std::string supplier, CommercialProposal proposal);
    void ComposeSupplyPlan(std::list<Supply> *supplyList, QDate beginDate, QDate endDate);

};




#endif // CONTRACT_H
