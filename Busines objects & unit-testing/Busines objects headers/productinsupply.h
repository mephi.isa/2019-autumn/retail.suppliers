#ifndef SUPPLYPRODUCT_H
#define SUPPLYPRODUCT_H

#include "product.h"
#include <QDate>
#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>

class ProductInSupply
{
private:

    Product InSupplyProductItem;
    int     InSupplyProductAmount;

public:
    ProductInSupply();
    ~ProductInSupply();
    int getAmount() const;
    void setAmount(int value);
    Product getProductItem() const;
    void setProductItem(const Product &value);
    bool isEmpty();
};

#endif // SUPPLYPRODUCT_H
