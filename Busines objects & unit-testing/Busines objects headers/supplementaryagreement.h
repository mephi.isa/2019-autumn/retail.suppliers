#ifndef SUPPLEMENTARYAGREEMENT_H
#define SUPPLEMENTARYAGREEMENT_H

#include "productsinsuppagr.h"
#include <QDate>
#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>

class SupplementaryAgreement
{
private:
      QDate       suppAgrDueDate;
      QDate       suppAgrSignDate;
      std::string suppAgrStatus;
      std::list<ProductsInSuppAgr> suppAgrProducts;
public:
    SupplementaryAgreement();
    ~SupplementaryAgreement();
    QDate getSuppAgrDueDate() const;
    void setSuppAgrDueDate(const QDate &value);
    QDate getSuppAgrSignDate() const;
    void setSuppAgrSignDate(const QDate &value);
    std::string getSuppAgrStatus() const;
    void setSuppAgrStatus(const std::string &value);
    std::list<ProductsInSuppAgr> getSuppAgrProducts() const;
    void setSuppAgrProducts(const std::list<ProductsInSuppAgr> &value);
    void ApproveAgreement(QDate signDate);
    void RejectAgreement(QDate signDate);
    void ComposeAgreement(QDate dueDate, std::list<ProductsInSuppAgr> *products);
     bool isEmpty();
};

#endif // SUPPLEMENTARYAGREEMENT_H
