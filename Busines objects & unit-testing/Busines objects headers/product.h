#ifndef PRODUCT_H
#define PRODUCT_H

#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>
#include <QDate>

class Product
{
private:
   int         productAnalyticID;
   std::string productCategoryName;
   std::string productItemNumber;
   std::string productDescription;
   std::string productConditions;



public:
    Product();
    ~Product();
    int getProductAnalyticID() const;
    void setProductAnalyticID(int value);
    std::string getProductCategoryName() const;
    void setProductCategoryName(const std::string &value);
    std::string getProductItemNumber() const;
    void setProductItemNumber(const std::string &value);
    std::string getProductDescription() const;
    void setProductDescription(const std::string &value);
    std::string getProductConditions() const;
    void setProductConditions(const std::string &value);

    bool isEmpty();
};

#endif // PRODUCT_H
