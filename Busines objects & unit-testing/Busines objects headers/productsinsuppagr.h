#ifndef PRODUCTSINSUPPAGR_H
#define PRODUCTSINSUPPAGR_H

#include "product.h"
#include <list>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <iomanip>
#include <ctime>
#include <QDate>

class ProductsInSuppAgr
{
private:
    Product psaProduct;
    int     psaAmount;
    double  psaPrice;

public:
    ProductsInSuppAgr();
    ~ProductsInSuppAgr();
    Product getPsaProduct() const;
    void setPsaProduct(const Product &value);
    int getPsaAmount() const;
    void setPsaAmount(int value);
    double getPsaPrice() const;
    void setPsaPrice(double value);
    void CreateProduct(Product category, int analityc, double price);
};

#endif // PRODUCTSINSUPPAGR_H
