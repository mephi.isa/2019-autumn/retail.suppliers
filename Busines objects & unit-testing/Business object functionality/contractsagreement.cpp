#include "contract.h"
#include "supplementaryagreement.h"
#include "product.h"
#include "productinsupply.h"
#include "productpull.h"
#include "productsinsuppagr.h"
#include "contract.h"
void Contract::AddSupplementaryAgreement(SupplementaryAgreement agreement)
{
    if(agreement.isEmpty()){
        std::cout<<"\nNo agreement.\n";
    }else{
        std::list <SupplementaryAgreement> sa_list;
        sa_list.push_back(agreement);
        this->setContractAgreements(sa_list);
    }
}

void Contract::RejectContract(QDate signDate)
{
    if(signDate.isNull()){
        std::cout<<"\nNo date.\n";
    }else{
    this->setContractSignDate(signDate);
       this->setContractStatus("rejected");
    }
}



void Contract::ApproveContract(QDate signDate)
{
    if(signDate.isNull()){
        std::cout<<"\nNo date.\n";
    }else{
    this->setContractSignDate(signDate);
       this->setContractStatus("approved");
    }
}

bool Contract::ComposeContract(int index, int empl_id, QDate startDate, QDate supplyDate, std::list<ProductPull> *prod_list, std::string supplier, CommercialProposal proposal)
{

   if(!(prod_list->empty()) && !(proposal.getProposalProducts().empty())){
      // std::cout<< prod_list->empty()<<"\n";
      // std::cout<<proposal.getProposalProducts().empty()<<"\n";
           this->setContractStartDate(startDate);
           this->setContractIndex(index);
           this->setContractEmployeeID(empl_id);
           this->setContractStartDate(startDate);
           this->setContractSupplyDate(supplyDate);
           this->setContractProducts(*prod_list);
           this->setContractSupplier(supplier);
           this->setContractProposal(proposal);
           return true;
   }else{
        std::cout<<"\nCannot compose contract. Too few data.\n";
        //std::cout<< prod_list->empty()<<"\n";
        //std::cout<<proposal.getProposalProducts().empty()<<"\n";
        return false;
    }



}




void Contract::ComposeSupplyPlan(std::list<Supply> *supplyList, QDate beginDate, QDate endDate)
{
    Supply s = Supply();
    std::list <ProductInSupply> supp_list;
    supp_list.clear();

    int g=0,j=0;
    ProductInSupply prod_in_supp = ProductInSupply();
    if(!this->getContractProducts().empty() && !endDate.isNull() && !beginDate.isNull()){

        s.setSupplyDueDate(endDate);//какая дата сюда должна ставиться?
        //дата начала не использется, где ее писать?

        for(auto iter: this->getContractProducts()){
        /*заполняем товарами из контракта*/
            g++;
            std::cout<<"g = "<<g<<"__"<<iter.getPullItem().getProductConditions()<<"\n";
            prod_in_supp.setProductItem(iter.getPullItem());
            std::cout<<"g = "<<g<<"__"<<iter.getPullItemAmount()<<"\n";
            prod_in_supp.setAmount(iter.getPullItemAmount());
            supp_list.push_back(prod_in_supp);
        }

        if(!this->getContractAgreements().empty()){
            for(auto iter: this->getContractAgreements()){
               for(auto i:iter.getSuppAgrProducts()){
                   j++;
                   std::cout<<"j = "<<j<<"__"<<iter.getSuppAgrStatus()<<"__"<<i.getPsaAmount()<<"\n";
                /*заполняем товарами из дополнительного соглашения*/
                prod_in_supp.setProductItem(i.getPsaProduct());
                prod_in_supp.setAmount(i.getPsaAmount());
                supp_list.push_back(prod_in_supp);
              }
           }
        }
         s.setSupplyProducts(supp_list);
         supplyList->push_back(s);
    }

}

