#include "productpull.h"

Product ProductPull::getPullItem() const
{
    return pullItem;
}

void ProductPull::setPullItem(const Product &value)
{
    pullItem = value;
}

int ProductPull::getPullItemAmount() const
{
    return pullItemAmount;
}

void ProductPull::setPullItemAmount(int value)
{
    pullItemAmount = value;
}

double ProductPull::getPullItemPrice() const
{
    return pullItemPrice;
}

void ProductPull::setPullItemPrice(double value)
{
    pullItemPrice = value;
}

void ProductPull::reduceProductPullAmount(int amount)
{
    pullItemAmount-=amount;
}

void ProductPull::raiseProductPullAmount(int amount)
{
    pullItemAmount+=amount;
}

ProductPull::ProductPull()
{

}

ProductPull::~ProductPull()
{

}
