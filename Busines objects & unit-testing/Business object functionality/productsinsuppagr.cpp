#include "productsinsuppagr.h"

Product ProductsInSuppAgr::getPsaProduct() const
{
    return psaProduct;
}

void ProductsInSuppAgr::setPsaProduct(const Product &value)
{
    psaProduct = value;
}

int ProductsInSuppAgr::getPsaAmount() const
{
    return psaAmount;
}

void ProductsInSuppAgr::setPsaAmount(int value)
{
    psaAmount = value;
}

double ProductsInSuppAgr::getPsaPrice() const
{
    return psaPrice;
}

void ProductsInSuppAgr::setPsaPrice(double value)
{
    psaPrice = value;
}

ProductsInSuppAgr::ProductsInSuppAgr()
{

}
ProductsInSuppAgr::~ProductsInSuppAgr()
{

}
