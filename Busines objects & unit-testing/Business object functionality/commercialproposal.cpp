#include "commercialproposal.h"

int CommercialProposal::getProposalIndex() const
{
    return proposalIndex;
}

void CommercialProposal::setProposalIndex(int value)
{
    proposalIndex = value;
}

int CommercialProposal::getProposalEmployeeID() const
{
    return proposalEmployeeID;
}

void CommercialProposal::setProposalEmployeeID(int value)
{
    proposalEmployeeID = value;
}

std::string CommercialProposal::getProposalSupplier() const
{
    return proposalSupplier;
}

void CommercialProposal::setProposalSupplier(const std::string &value)
{
    proposalSupplier = value;
}

QDate CommercialProposal::getProposalDueDate() const
{
    return proposalDueDate;
}

void CommercialProposal::setProposalDueDate(const QDate &value)
{
    proposalDueDate = value;
}

QDate CommercialProposal::getProposalStartDate() const
{
    return proposalStartDate;
}

void CommercialProposal::setProposalStartDate(const QDate &value)
{
    proposalStartDate = value;
}

QDate CommercialProposal::getProposalSignDate() const
{
    return proposalSignDate;
}

void CommercialProposal::setProposalSignDate(const QDate &value)
{
    proposalSignDate = value;
}

std::string CommercialProposal::getProposalStatus() const
{
    return proposalStatus;
}

void CommercialProposal::setProposalStatus(const std::string &value)
{
    proposalStatus = value;
}

std::list<ProductPull> CommercialProposal::getProposalProducts() const
{
    return proposalProducts;
}

void CommercialProposal::setProposalProducts(const std::list<ProductPull> &value)
{
    proposalProducts = value;
}

CommercialProposal::CommercialProposal()
{

}
CommercialProposal::~CommercialProposal()
{

}
