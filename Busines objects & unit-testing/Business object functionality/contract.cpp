#include "contract.h"

CommercialProposal Contract::getContractProposal() const
{
    return contractProposal;
}

void Contract::setContractProposal(const CommercialProposal &value)
{
    contractProposal = value;
}

std::list<SupplementaryAgreement> Contract::getContractAgreements() const
{
    return contractAgreements;
}

void Contract::setContractAgreements(const std::list<SupplementaryAgreement> &value)
{
    contractAgreements = value;
}

Contract::Contract(int index,QDate startDate, QDate dueDate,std::string status)
{
    contractIndex=index;
    contractStartDate=startDate;
    contractDueDate=dueDate;
    contractStatus=status;
    
}

Contract::~Contract()
{
    
}

std::list<ProductPull> Contract::getContractProducts() const
{
    return contractProducts;
}

void Contract::setContractProducts(const std::list<ProductPull> &value)
{
    contractProducts = value;
}

std::string Contract::getContractStatus() const
{
    return contractStatus;
}

void Contract::setContractStatus(const std::string &value)
{
    contractStatus = value;
}

QDate Contract::getContractSignDate() const
{
    return contractSignDate;
}

void Contract::setContractSignDate(const QDate &value)
{
    contractSignDate = value;
}

QDate Contract::getContractSupplyDate() const
{
    return contractSupplyDate;
}

void Contract::setContractSupplyDate(const QDate &value)
{
    contractSupplyDate = value;
}

QDate Contract::getContractDueDate() const
{
    return contractDueDate;
}

void Contract::setContractDueDate(const QDate &value)
{
    contractDueDate = value;
}

QDate Contract::getContractStartDate() const
{
    return contractStartDate;
}

void Contract::setContractStartDate(const QDate &value)
{
    contractStartDate = value;
}

std::string Contract::getContractSupplier() const
{
    return contractSupplier;
}

void Contract::setContractSupplier(const std::string &value)
{
    contractSupplier = value;
}

int Contract::getContractEmployeeID() const
{
    return contractEmployeeID;
}

void Contract::setContractEmployeeID(int value)
{
    contractEmployeeID = value;
}

int Contract::getContractIndex() const
{
    return contractIndex;
}

void Contract::setContractIndex(int value)
{
    contractIndex = value;
}


