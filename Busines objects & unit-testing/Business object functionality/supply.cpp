#include "supply.h"

QDate Supply::getSupplyDueDate() const
{
    return supplyDueDate;
}

void Supply::setSupplyDueDate(const QDate &value)
{
    supplyDueDate = value;
}

std::list<ProductInSupply> Supply::getSupplyProducts() const
{
    return supplyProducts;
}

void Supply::setSupplyProducts(const std::list<ProductInSupply> &value)
{
    supplyProducts = value;
}

Supply::Supply()
{

}

Supply::~Supply()
{

}
