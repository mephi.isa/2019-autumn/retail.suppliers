#include "product.h"

int Product::getProductAnalyticID() const
{
    return productAnalyticID;
}

void Product::setProductAnalyticID(int value)
{
    productAnalyticID = value;
}

std::string Product::getProductCategoryName() const
{
    return productCategoryName;
}

void Product::setProductCategoryName(const std::string &value)
{
    productCategoryName = value;
}

std::string Product::getProductItemNumber() const
{
    return productItemNumber;
}

void Product::setProductItemNumber(const std::string &value)
{
    productItemNumber = value;
}

std::string Product::getProductDescription() const
{
    return productDescription;
}

void Product::setProductDescription(const std::string &value)
{
    productDescription = value;
}

std::string Product::getProductConditions() const
{
    return productConditions;
}

void Product::setProductConditions(const std::string &value)
{
    productConditions = value;
}

Product::Product()
{

}

Product::~Product()
{

}

bool Product::isEmpty()
{
    if(productAnalyticID == 0 || productCategoryName.empty() || productItemNumber.empty() || productDescription.empty() || productConditions.empty())
    return true;
    return false;
}
