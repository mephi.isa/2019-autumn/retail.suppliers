#include "productinsupply.h"

int ProductInSupply::getAmount() const
{
    return InSupplyProductAmount;
}

void ProductInSupply::setAmount(int value)
{
    InSupplyProductAmount = value;
}

Product ProductInSupply::getProductItem() const
{
    return InSupplyProductItem;
}

void ProductInSupply::setProductItem(const Product &value)
{
    InSupplyProductItem = value;
}

bool ProductInSupply::isEmpty()
{
    if(InSupplyProductAmount == 0 || InSupplyProductItem.isEmpty())
        return true;
        return false;
}

ProductInSupply::ProductInSupply()
{

}

ProductInSupply::~ProductInSupply()
{

}
