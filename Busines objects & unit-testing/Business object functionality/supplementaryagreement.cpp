#include "supplementaryagreement.h"

QDate SupplementaryAgreement::getSuppAgrDueDate() const
{
    return suppAgrDueDate;
}

void SupplementaryAgreement::setSuppAgrDueDate(const QDate &value)
{
    suppAgrDueDate = value;
}

QDate SupplementaryAgreement::getSuppAgrSignDate() const
{
    return suppAgrSignDate;
}

void SupplementaryAgreement::setSuppAgrSignDate(const QDate &value)
{
    suppAgrSignDate = value;
}

std::string SupplementaryAgreement::getSuppAgrStatus() const
{
    return suppAgrStatus;
}

void SupplementaryAgreement::setSuppAgrStatus(const std::string &value)
{
    suppAgrStatus = value;
}

std::list<ProductsInSuppAgr> SupplementaryAgreement::getSuppAgrProducts() const
{
    return suppAgrProducts;
}

void SupplementaryAgreement::setSuppAgrProducts(const std::list<ProductsInSuppAgr> &value)
{
    suppAgrProducts = value;
}

bool SupplementaryAgreement::isEmpty()
{
    if(suppAgrDueDate.isNull() || suppAgrStatus.empty() || suppAgrProducts.empty())
        return true;
        return false;
}

SupplementaryAgreement::SupplementaryAgreement()
{
  // suppAgrDueDate.setDate(0,0,0);
   //suppAgrSignDate.setDate(0,0,0);
   suppAgrStatus.clear();
}

SupplementaryAgreement::~SupplementaryAgreement()
{

}
