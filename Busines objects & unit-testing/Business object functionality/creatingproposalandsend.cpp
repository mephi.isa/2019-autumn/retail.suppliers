#include "commercialproposal.h"

std::string CommercialProposal::MakeProposal(int index, int empl_id, std::list<ProductPull> *product_list, std::string provider, QDate dd, QDate sd){
    std::cout << "\nCompose new commercial proposal.\n";

    if(index==0||empl_id==0||product_list->empty()||provider.empty()||dd.isNull()||sd.isNull()){
         std::cout << "\nCannot compose new commercial proposal.\nEmpty input data.\n";
        return "";
    }else{
        this->setProposalIndex(index);
        this->setProposalEmployeeID(empl_id);
        this->setProposalProducts(*product_list);
        this->setProposalStartDate(sd);
        this->setProposalDueDate(dd);
        this->setProposalSupplier(provider);
        return "processing";
       }

}

void CommercialProposal::RejectCommercialProposal(QDate signDate){
    this->setProposalSignDate(signDate);
    this->setProposalStatus("rejected");
}

void CommercialProposal::ApproveCommercialProposal(QDate signDate){
    this->setProposalSignDate(signDate);
    this->setProposalStatus("approved");
}
