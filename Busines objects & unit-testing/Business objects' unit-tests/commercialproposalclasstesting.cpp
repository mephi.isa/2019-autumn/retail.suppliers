#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"


TEST(CommercialProposalTest,ProductsFilled){
    QDate d,s,st;
    CommercialProposal prop;
    ProductPull pull;
    std::list <ProductPull> prp;
    Product p;

    p.setProductAnalyticID(56);
    p.setProductCategoryName("x");
    p.setProductConditions("conditions");
    p.setProductDescription("description");
    p.setProductItemNumber("67");

    d.setDate(2019,12,3);
    s.setDate(2019,10,8);
    st.setDate(2019,10,20);

    pull.setPullItem(p);
    pull.setPullItemAmount(500);
    pull.setPullItemPrice(300.78);

    prp.push_back(pull);

    prop.setProposalProducts(prp);

    EXPECT_EQ(500,prop.getProposalProducts().back().getPullItemAmount());
    EXPECT_EQ(300.78,prop.getProposalProducts().back().getPullItemPrice());
    EXPECT_EQ(56,prop.getProposalProducts().back().getPullItem().getProductAnalyticID());
    EXPECT_EQ("x",prop.getProposalProducts().back().getPullItem().getProductCategoryName());
    EXPECT_EQ("description",prop.getProposalProducts().back().getPullItem().getProductDescription());
    EXPECT_EQ("conditions",prop.getProposalProducts().back().getPullItem().getProductConditions());
    EXPECT_EQ("67",prop.getProposalProducts().back().getPullItem().getProductItemNumber());
}



TEST(CommercialProposalTest, ProductsEmpty){
    CommercialProposal prop;
    std::list <ProductPull> prp;
    prop.setProposalProducts(prp);
    EXPECT_TRUE(prop.getProposalProducts().empty());
}


TEST(CommercialProposalTest, EmployeeIDTest){
    CommercialProposal prop;
    prop.setProposalEmployeeID(678);
    EXPECT_EQ(678, prop.getProposalEmployeeID());
}

TEST(CommercialProposalTest, IndexTest){
    CommercialProposal prop;
    prop.setProposalIndex(567);
    EXPECT_EQ(567, prop.getProposalIndex());
}

TEST(CommercialProposalTest, StatusTest){
    CommercialProposal prop;
    prop.setProposalStatus("waiting");
    EXPECT_EQ("waiting", prop.getProposalStatus());
}


TEST(CommercialProposalTest, SupplierTest){
    CommercialProposal prop;
    prop.setProposalSupplier("ccc");
    EXPECT_EQ("ccc", prop.getProposalSupplier());
}


TEST(CommercialProposalTest, DueDateTestFilled){
    QDate d = QDate(2019,6,17);
    CommercialProposal prop;
    prop.setProposalDueDate(d);
    EXPECT_EQ(d, prop.getProposalDueDate());
}


TEST(CommercialProposalTest, SignDateTestFilled){
    QDate d = QDate(2019,6,17);
    CommercialProposal prop;
    prop.setProposalSignDate(d);
    EXPECT_EQ(d, prop.getProposalSignDate());
}


TEST(CommercialProposalTest, StartDateTestFilled){
    QDate d = QDate(2019,6,17);
    CommercialProposal prop;
    prop.setProposalStartDate(d);
    EXPECT_EQ(d, prop.getProposalStartDate());
}


TEST(CommercialProposalTest, DueDateTestEmpty){
    QDate d;
    CommercialProposal prop;
    prop.setProposalDueDate(d);
    EXPECT_TRUE(prop.getProposalDueDate().isNull());
}



TEST(CommercialProposalTest, SignDateTestEmpty){
    QDate d;
    CommercialProposal prop;
    prop.setProposalSignDate(d);
    EXPECT_TRUE(prop.getProposalSignDate().isNull());
}


TEST(CommercialProposalTest, StartDateTestEmpty){
    QDate d;
    CommercialProposal prop;
    prop.setProposalStartDate(d);
    EXPECT_TRUE(prop.getProposalStartDate().isNull());
}








