#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"



TEST(ContractTest, SupplyPlanTest1){
    QDate d1,d2,d3;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
     d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    SupplementaryAgreement sa = SupplementaryAgreement();
    d1.setDate(2019,7,16);
    d2.setDate(2019,8,1);
    sa.setSuppAgrDueDate(d2);
    sa.setSuppAgrSignDate(d1);
    sa.setSuppAgrStatus("waiting");
    ProductsInSuppAgr psa = ProductsInSuppAgr();
    Product pr = Product();
    pr.setProductAnalyticID(456);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    psa.setPsaAmount(200);
    psa.setPsaPrice(134.50);
    psa.setPsaProduct(pr);
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    com_prop.setProposalDueDate(d2);
    com_prop.setProposalEmployeeID(2);
    com_prop.setProposalIndex(3);
    com_prop.setProposalStartDate(d1);
    com_prop.setProposalStatus("waiting");
    com_prop.setProposalSupplier("supplier");
    com_prop.setProposalProducts(prod);
    c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop);

    std::list <ProductsInSuppAgr> psagr;
    psagr.push_back(psa);
    sa.setSuppAgrProducts(psagr);
    c.AddSupplementaryAgreement(sa);

    std::list <Supply> supp;
    supp.clear();
    d1.setDate(2019,7,15);
    d2.setDate(2019,9,5);
    c.ComposeSupplyPlan(&supp,d1,d2);
    std::cout<<supp.empty()<<"\n";
    std::cout<<supp.back().getSupplyProducts().back().getAmount()<<"\n";
    EXPECT_FALSE(supp.empty());
    EXPECT_FALSE(sa.getSuppAgrProducts().empty());
    EXPECT_FALSE(c.getContractAgreements().empty());
}



TEST(ContractTest, SupplyPlanTest2){
    QDate d1,d2,d3;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
     d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    d1.setDate(2019,7,16);
    d2.setDate(2019,8,1);
    ProductsInSuppAgr psa = ProductsInSuppAgr();
    Product pr = Product();
    pr.setProductAnalyticID(456);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    psa.setPsaAmount(200);
    psa.setPsaPrice(134.50);
    psa.setPsaProduct(pr);
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    com_prop.setProposalDueDate(d2);
    com_prop.setProposalEmployeeID(2);
    com_prop.setProposalIndex(3);
    com_prop.setProposalStartDate(d1);
    com_prop.setProposalStatus("waiting");
    com_prop.setProposalSupplier("supplier");
    com_prop.setProposalProducts(prod);
    c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop);
    std::list <Supply> supp;
    supp.clear();
    d1.setDate(2019,7,15);
    d2.setDate(2019,9,5);
    c.ComposeSupplyPlan(&supp,d1,d2);
    std::cout<<supp.empty()<<"\n";
    std::cout<<supp.back().getSupplyProducts().back().getAmount()<<"\n";
    EXPECT_FALSE(supp.empty());
}


TEST(ContractTest, SupplyPlanTestEmpty){
    QDate d1,d2,d3;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    Contract c = Contract(1,d1,d2,"waiting");
    std::list <Supply> supp;
    supp.clear();
    d1.setDate(2019,7,15);
    d2.setDate(2019,9,5);
    c.ComposeSupplyPlan(&supp,d1,d2);
    std::cout<<supp.empty()<<"\n";
    EXPECT_TRUE(supp.empty());

}


TEST(ContractTest, SupplyPlanTestEmptyDate1){
    QDate d1,d2,d3,d4;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
     d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    d1.setDate(2019,7,16);
    d2.setDate(2019,8,1);
    ProductsInSuppAgr psa = ProductsInSuppAgr();
    Product pr = Product();
    pr.setProductAnalyticID(456);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    psa.setPsaAmount(200);
    psa.setPsaPrice(134.50);
    psa.setPsaProduct(pr);
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    com_prop.setProposalDueDate(d2);
    com_prop.setProposalEmployeeID(2);
    com_prop.setProposalIndex(3);
    com_prop.setProposalStartDate(d1);
    com_prop.setProposalStatus("waiting");
    com_prop.setProposalSupplier("supplier");
    com_prop.setProposalProducts(prod);
    c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop);
    std::list <Supply> supp;
    supp.clear();
    c.ComposeSupplyPlan(&supp,d1,d4);
    std::cout<<supp.empty()<<"\n";
    EXPECT_TRUE(supp.empty());
}


TEST(ContractTest, SupplyPlanTestEmptyDate2){
    QDate d1,d2,d3,d4;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    d1.setDate(2019,7,16);
    d2.setDate(2019,8,1);
    ProductsInSuppAgr psa = ProductsInSuppAgr();
    Product pr = Product();
    pr.setProductAnalyticID(456);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    psa.setPsaAmount(200);
    psa.setPsaPrice(134.50);
    psa.setPsaProduct(pr);
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    com_prop.setProposalDueDate(d2);
    com_prop.setProposalEmployeeID(2);
    com_prop.setProposalIndex(3);
    com_prop.setProposalStartDate(d1);
    com_prop.setProposalStatus("waiting");
    com_prop.setProposalSupplier("supplier");
    com_prop.setProposalProducts(prod);
    c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop);
    std::list <Supply> supp;
    supp.clear();
    c.ComposeSupplyPlan(&supp,d4,d1);
    std::cout<<supp.empty()<<"\n";
    EXPECT_TRUE(supp.empty());

}



TEST(ContractTest, AddSuppAgrTestFilled){
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    Contract c = Contract(1,d1,d2,"waiting");
    SupplementaryAgreement sa = SupplementaryAgreement();
    d1.setDate(2019,7,16);
    d2.setDate(2019,8,1);
    sa.setSuppAgrDueDate(d2);
    sa.setSuppAgrSignDate(d1);
    sa.setSuppAgrStatus("waiting");
    ProductsInSuppAgr psa = ProductsInSuppAgr();
    Product pr = Product();
    pr.setProductAnalyticID(456);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    psa.setPsaAmount(200);
    psa.setPsaPrice(134.50);
    psa.setPsaProduct(pr);

   std::list <ProductsInSuppAgr> psagr;
    psagr.push_back(psa);

    sa.setSuppAgrProducts(psagr);

    d1.setDate(2019,7,15);
    c.AddSupplementaryAgreement(sa);
    EXPECT_FALSE(sa.getSuppAgrProducts().empty());
    EXPECT_FALSE(c.getContractAgreements().empty());
}


TEST(ContractTest,ComposeContractTest){
    QDate d1,d2,d3;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);

    com_prop.setProposalDueDate(d2);
    com_prop.setProposalEmployeeID(2);
    com_prop.setProposalIndex(3);
    com_prop.setProposalStartDate(d1);
    com_prop.setProposalStatus("waiting");
    com_prop.setProposalSupplier("supplier");
    com_prop.setProposalProducts(prod);

    ASSERT_TRUE(c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop));
    EXPECT_EQ(d2,c.getContractDueDate());
    EXPECT_EQ(d1,c.getContractStartDate());
    EXPECT_EQ(d3,c.getContractSupplyDate());
    EXPECT_EQ(4,c.getContractProducts().back().getPullItem().getProductAnalyticID());
    EXPECT_EQ("product",c.getContractProducts().back().getPullItem().getProductCategoryName());
    EXPECT_EQ("cond",c.getContractProducts().back().getPullItem().getProductConditions());
    EXPECT_EQ("descr",c.getContractProducts().back().getPullItem().getProductDescription());
    EXPECT_EQ("1",c.getContractProducts().back().getPullItem().getProductItemNumber());
    EXPECT_EQ(123,c.getContractProducts().back().getPullItemAmount());
    EXPECT_EQ(456,c.getContractProducts().back().getPullItemPrice());
    EXPECT_EQ(d2,c.getContractProposal().getProposalDueDate());
    EXPECT_EQ(2,c.getContractProposal().getProposalEmployeeID());
    EXPECT_EQ(3,c.getContractProposal().getProposalIndex());
    EXPECT_EQ(d1,c.getContractProposal().getProposalStartDate());
    EXPECT_EQ("waiting",c.getContractProposal().getProposalStatus());
    EXPECT_EQ("supplier",c.getContractProposal().getProposalSupplier());
    EXPECT_TRUE(c.getContractAgreements().empty());

}


TEST(ContractTest, ComposeContractTestEmpty1){
    QDate d1,d2,d3;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
    Product pr = Product();
    std::list <ProductPull> prod;
    prod.clear();
    ASSERT_FALSE(c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop));
    EXPECT_TRUE(c.getContractAgreements().empty());
    EXPECT_TRUE(c.getContractProducts().empty());
    EXPECT_TRUE(c.getContractProposal().getProposalProducts().empty());
    EXPECT_TRUE(c.getContractProposal().getProposalDueDate().isNull());
    EXPECT_TRUE(c.getContractProposal().getProposalSignDate().isNull());
    EXPECT_TRUE(c.getContractProposal().getProposalStartDate().isNull());
    EXPECT_EQ("",c.getContractProposal().getProposalStatus());
    EXPECT_EQ("",c.getContractProposal().getProposalSupplier());
}


TEST(ContractTest, ComposeContractTestEmpty2){
    QDate d1,d2,d3;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
    Product pr = Product();
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    ASSERT_FALSE(c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop));
    EXPECT_TRUE(c.getContractAgreements().empty());
    EXPECT_TRUE(c.getContractProducts().empty());
    EXPECT_TRUE(c.getContractProposal().getProposalProducts().empty());
    EXPECT_TRUE(c.getContractProposal().getProposalDueDate().isNull());
    EXPECT_TRUE(c.getContractProposal().getProposalSignDate().isNull());
    EXPECT_TRUE(c.getContractProposal().getProposalStartDate().isNull());
    EXPECT_EQ("",c.getContractProposal().getProposalStatus());
    EXPECT_EQ("",c.getContractProposal().getProposalSupplier());
}

TEST(ContractTest, ComposeContractTestEmpty3){
    QDate d1,d2,d3;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    d3.setDate(2019,9,12);
    Contract c = Contract(1,d1,d2,"waiting");
    CommercialProposal com_prop = CommercialProposal();
    ProductPull prp = ProductPull();
        Product pr = Product();
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    com_prop.setProposalProducts(prod);
    prod.clear();
    ASSERT_FALSE(c.ComposeContract(1,2,d1,d3,&prod,"supplier",com_prop));
    EXPECT_TRUE(c.getContractAgreements().empty());
    EXPECT_TRUE(c.getContractProducts().empty());
    EXPECT_TRUE(c.getContractProposal().getProposalProducts().empty());
    EXPECT_TRUE(c.getContractProposal().getProposalDueDate().isNull());
    EXPECT_TRUE(c.getContractProposal().getProposalSignDate().isNull());
    EXPECT_TRUE(c.getContractProposal().getProposalStartDate().isNull());
    EXPECT_EQ("",c.getContractProposal().getProposalStatus());
    EXPECT_EQ("",c.getContractProposal().getProposalSupplier());
}


TEST(ContractTest, RejectTest){
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    Contract c = Contract(1,d1,d2,"waiting");
    d1.setDate(2019,7,7);
    c.RejectContract(d1);
    EXPECT_EQ(d1,c.getContractSignDate());
    EXPECT_EQ("rejected",c.getContractStatus());
}

TEST(ContractTest, ApproveTest){
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    Contract c = Contract(1,d1,d2,"waiting");
    d1.setDate(2019,7,7);
    c.ApproveContract(d1);
    EXPECT_EQ(d1,c.getContractSignDate());
    EXPECT_EQ("approved",c.getContractStatus());
}

TEST(CommercialProposalTest, RejectTest){
    CommercialProposal cp = CommercialProposal();
    QDate d1;
    d1.setDate(2019,6,7);
    cp.RejectCommercialProposal(d1);
    EXPECT_EQ(d1,cp.getProposalSignDate());
    EXPECT_EQ("rejected",cp.getProposalStatus());
}


TEST(CommercialProposalTest,ApproveTest){
    CommercialProposal cp = CommercialProposal();
    QDate d1;
    d1.setDate(2019,6,7);
    cp.ApproveCommercialProposal(d1);
    EXPECT_EQ(d1,cp.getProposalSignDate());
    EXPECT_EQ("approved",cp.getProposalStatus());
}

TEST(CommercialProposalTest, CreateProposalTestFilled){
    CommercialProposal cp = CommercialProposal();
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    EXPECT_EQ("processing",cp.MakeProposal(56,34,&prod,"provider",d1,d2));

    EXPECT_EQ(56,cp.getProposalIndex());
    EXPECT_EQ(34,cp.getProposalEmployeeID());
    EXPECT_FALSE(cp.getProposalProducts().empty());
    EXPECT_EQ(d2,cp.getProposalStartDate());
    EXPECT_EQ(d1,cp.getProposalDueDate());
    EXPECT_EQ("provider",cp.getProposalSupplier());
}

TEST(CommercialProposalTest, CreateProposalTestEmpty1){
    CommercialProposal cp = CommercialProposal();
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    //prod.push_back(prp);
    EXPECT_EQ("",cp.MakeProposal(56,34,&prod,"provider",d1,d2));
    EXPECT_TRUE(cp.getProposalProducts().empty());
    EXPECT_EQ("",cp.getProposalSupplier());
}

TEST(CommercialProposalTest, CreateProposalTestEmpty2){
    CommercialProposal cp = CommercialProposal();
    QDate d1,d2;
    d1.setDate(2019,6,7);
   // d2.setDate(2019,7,16);
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    EXPECT_EQ("",cp.MakeProposal(56,34,&prod,"provider",d1,d2));
    EXPECT_TRUE(cp.getProposalProducts().empty());
    EXPECT_EQ("",cp.getProposalSupplier());
}

TEST(CommercialProposalTest, CreateProposalTestEmpty3){
    CommercialProposal cp = CommercialProposal();
    QDate d1,d2;
    //d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    EXPECT_EQ("",cp.MakeProposal(56,34,&prod,"provider",d1,d2));
    EXPECT_TRUE(cp.getProposalProducts().empty());
    EXPECT_EQ("",cp.getProposalSupplier());
}

TEST(CommercialProposalTest, CreateProposalTestEmpty4){
    CommercialProposal cp = CommercialProposal();
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    EXPECT_EQ("",cp.MakeProposal(56,34,&prod,"",d1,d2));
    EXPECT_TRUE(cp.getProposalProducts().empty());
    EXPECT_EQ("",cp.getProposalSupplier());
}

TEST(CommercialProposalTest, CreateProposalTestEmpty5){
    CommercialProposal cp = CommercialProposal();
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    EXPECT_EQ("",cp.MakeProposal(56,0,&prod,"provider",d1,d2));
    EXPECT_TRUE(cp.getProposalProducts().empty());
    EXPECT_EQ("",cp.getProposalSupplier());
}


TEST(CommercialProposalTest, CreateProposalTestEmpty6){
    CommercialProposal cp = CommercialProposal();
    QDate d1,d2;
    d1.setDate(2019,6,7);
    d2.setDate(2019,7,16);
    ProductPull prp = ProductPull();
    Product pr = Product();
    pr.setProductAnalyticID(4);
    pr.setProductItemNumber("1");
    pr.setProductCategoryName("product");
    pr.setProductConditions("cond");
    pr.setProductDescription("descr");
    prp.setPullItemPrice(456);
    prp.setPullItemAmount(123);
    prp.setPullItem(pr);
    std::list <ProductPull> prod;
    prod.clear();
    prod.push_back(prp);
    EXPECT_EQ("",cp.MakeProposal(0,34,&prod,"provider",d1,d2));
    EXPECT_TRUE(cp.getProposalProducts().empty());
}
