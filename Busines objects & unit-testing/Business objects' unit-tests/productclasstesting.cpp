#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"


TEST(ProductTest, AnanlyticIDTest){
    Product pr = Product();
    pr.setProductAnalyticID(18923);
    EXPECT_EQ(18923, pr.getProductAnalyticID());
}

TEST(ProductTest, CategoryNameTest ){
    Product pr = Product();
    pr.setProductCategoryName("blue cheez");
    EXPECT_EQ("blue cheez", pr.getProductCategoryName());
}

TEST(ProductTest, ItemNumberTest){
    Product pr = Product();
    pr.setProductItemNumber("489CES");
    EXPECT_EQ("489CES", pr.getProductItemNumber());
}

TEST(ProductTest, DescriptionTest){
    Product pr = Product();
    pr.setProductDescription("cheez with blue mold");
    EXPECT_EQ("cheez with blue mold", pr.getProductDescription());
}

TEST(ProductTest, ConditionsTest){
    Product pr = Product();
    pr.setProductConditions("only natural");
    EXPECT_EQ("only natural", pr.getProductConditions());
}

