#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"


TEST(ProductPullTest,ItemTest){
    ProductPull pr = ProductPull();
    Product item =Product();
    item.setProductAnalyticID(12345);
    item.setProductItemNumber("1");
    item.setProductCategoryName("white cheez");
    item.setProductConditions("conditions");
    item.setProductDescription("brine");
    pr.setPullItem(item);
    EXPECT_EQ("1",pr.getPullItem().getProductItemNumber());
    EXPECT_EQ("white cheez",pr.getPullItem().getProductCategoryName());
    EXPECT_EQ("conditions",pr.getPullItem().getProductConditions());
    EXPECT_EQ("brine",pr.getPullItem().getProductDescription());
    EXPECT_EQ(12345,pr.getPullItem().getProductAnalyticID());
}

TEST(ProductPullTest,ItemAmountTest){
    ProductPull pr = ProductPull();
    pr.setPullItemAmount(900);
    EXPECT_EQ(900,pr.getPullItemAmount());
}

TEST(ProductPullTest,ItemPriceTest){
    ProductPull pr = ProductPull();
    pr.setPullItemPrice(1400.80);
    EXPECT_DOUBLE_EQ(1400.80,pr.getPullItemPrice());
}

TEST(ProductPullTest,ReduceItemAmountTest){
ProductPull pp = ProductPull();
int a;
pp.setPullItemAmount(100);
a=pp.getPullItemAmount();
pp.reduceProductPullAmount(50);
EXPECT_LT(pp.getPullItemAmount(),a);
EXPECT_EQ(50,pp.getPullItemAmount());
}

TEST(ProductPullTest,RaiseItemAmountTest){
    ProductPull pp = ProductPull();
    int a;
    pp.setPullItemAmount(100);
    a=pp.getPullItemAmount();
    pp.raiseProductPullAmount(50);
    EXPECT_GT(pp.getPullItemAmount(),a);
    EXPECT_EQ(150,pp.getPullItemAmount());
}
