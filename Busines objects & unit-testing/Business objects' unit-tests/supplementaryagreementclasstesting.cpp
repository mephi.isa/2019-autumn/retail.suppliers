#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"




TEST(SupplementaryAgreementTest, ProductsListTestFilled){
SupplementaryAgreement sa = SupplementaryAgreement();
ProductsInSuppAgr p = ProductsInSuppAgr();
Product item =Product();
std::list <ProductsInSuppAgr> psa;
item.setProductAnalyticID(12345);
item.setProductItemNumber("1");
item.setProductCategoryName("white cheez");
item.setProductConditions("conditions");
item.setProductDescription("brine");
p.setPsaAmount(600);
p.setPsaPrice(1600.50);
p.setPsaProduct(item);
psa.push_back(p);
sa.setSuppAgrProducts(psa);
EXPECT_EQ(600,psa.back().getPsaAmount());
EXPECT_DOUBLE_EQ(1600.50,psa.back().getPsaPrice());
EXPECT_EQ(12345,psa.back().getPsaProduct().getProductAnalyticID());
EXPECT_EQ("1",psa.back().getPsaProduct().getProductItemNumber());
EXPECT_EQ("white cheez",psa.back().getPsaProduct().getProductCategoryName());
EXPECT_EQ("conditions",psa.back().getPsaProduct().getProductConditions());
EXPECT_EQ("brine",psa.back().getPsaProduct().getProductDescription());
}

TEST(SupplementaryAgreementTest, ProductsListTestEmpty){
SupplementaryAgreement sa = SupplementaryAgreement();
std::list <ProductsInSuppAgr> psa;
sa.setSuppAgrProducts(psa);
EXPECT_TRUE(sa.getSuppAgrProducts().empty());
}

TEST(SupplementaryAgreementTest, StatusTestFilled){
SupplementaryAgreement sa = SupplementaryAgreement();
sa.setSuppAgrStatus("approved");
EXPECT_EQ("approved",sa.getSuppAgrStatus());
}


TEST(SupplementaryAgreementTest, StatusTestEmpty){
SupplementaryAgreement sa = SupplementaryAgreement();
EXPECT_TRUE(sa.getSuppAgrStatus().empty());
}



TEST(SupplementaryAgreementTest, DueDateTestFilled){
SupplementaryAgreement sa = SupplementaryAgreement();
QDate dd;
dd.setDate(2019,10,02);
sa.setSuppAgrDueDate(dd);
EXPECT_EQ(dd,sa.getSuppAgrDueDate());
}
TEST(SupplementaryAgreementTest, DueDateTestEmpty){
SupplementaryAgreement sa = SupplementaryAgreement();
QDate dd;
sa.setSuppAgrDueDate(dd);
EXPECT_TRUE(sa.getSuppAgrDueDate().isNull());
}

TEST(SupplementaryAgreementTest,SignDateTestFilled){
    SupplementaryAgreement sa = SupplementaryAgreement();
    QDate dd;
    dd.setDate(2019,10,02);
    sa.setSuppAgrSignDate(dd);
    EXPECT_EQ(dd,sa.getSuppAgrSignDate());
}

TEST(SupplementaryAgreementTest,SignDateTestEmpty){
    SupplementaryAgreement sa = SupplementaryAgreement();
    QDate dd;
    sa.setSuppAgrSignDate(dd);
    EXPECT_TRUE(sa.getSuppAgrSignDate().isNull());
}
/*
TEST(SupplementaryAgreementTest, CreateAgreementTest){


}

*/
