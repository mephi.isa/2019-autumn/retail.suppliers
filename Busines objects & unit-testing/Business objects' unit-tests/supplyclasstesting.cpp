#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"


TEST(SupplyTest,DueDateTest){
    Supply obj= Supply();
    QDate date = QDate(2019,06,10);
    obj.setSupplyDueDate(date);
    obj.getSupplyDueDate();
    EXPECT_EQ(date, obj.getSupplyDueDate());
}



TEST(SupplyTest,ProductListTestEmpty){
    Supply obj= Supply();
    std::list <ProductInSupply> products;
    obj.setSupplyProducts(products);
    EXPECT_TRUE(obj.getSupplyProducts().empty());
}

TEST(SupplyTest,ProductListTestFilled){
    Supply obj= Supply();
    std::list <ProductInSupply> products;
    ProductInSupply product = ProductInSupply();
    Product item = Product();
    item.setProductAnalyticID(12345);
    item.setProductItemNumber("1");
    item.setProductCategoryName("white cheez");
    item.setProductConditions("conditions");
    item.setProductDescription("brine");
    product.setProductItem(item);
    product.setAmount(500);
    products.push_back(product);
    obj.setSupplyProducts(products);

    EXPECT_FALSE(obj.getSupplyProducts().empty());
    EXPECT_EQ(500,obj.getSupplyProducts().back().getAmount());
    EXPECT_EQ(12345,obj.getSupplyProducts().back().getProductItem().getProductAnalyticID());
    EXPECT_EQ("white cheez",obj.getSupplyProducts().back().getProductItem().getProductCategoryName());
    EXPECT_EQ("conditions",obj.getSupplyProducts().back().getProductItem().getProductConditions());
    EXPECT_EQ("brine",obj.getSupplyProducts().back().getProductItem().getProductDescription());
}







