#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"

TEST(ProductsInSuppAgrTest, AmountTest){
    ProductsInSuppAgr pr = ProductsInSuppAgr();
    pr.setPsaAmount(700);
    EXPECT_EQ(700,pr.getPsaAmount());
}

TEST(ProductsInSuppAgrTest, PriceTest){
    ProductsInSuppAgr pr = ProductsInSuppAgr();
    pr.setPsaPrice(192.24);
    EXPECT_EQ(192.24,pr.getPsaPrice());
}
TEST(ProductsInSuppAgrTest, ProductTest){
    ProductsInSuppAgr pr = ProductsInSuppAgr();
    Product item = Product();
    item.setProductAnalyticID(12345);
    item.setProductItemNumber("1");
    item.setProductCategoryName("white cheez");
    item.setProductConditions("conditions");
    item.setProductDescription("brine");
    pr.setPsaProduct(item);
    EXPECT_EQ(12345,pr.getPsaProduct().getProductAnalyticID());
    EXPECT_EQ("1",pr.getPsaProduct().getProductItemNumber());
    EXPECT_EQ("white cheez",pr.getPsaProduct().getProductCategoryName());
    EXPECT_EQ("conditions",pr.getPsaProduct().getProductConditions());
    EXPECT_EQ("brine",pr.getPsaProduct().getProductDescription());
}




