#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"

TEST(ProducInSupplyTest, AmountTest){
    ProductInSupply product = ProductInSupply();
    product.setAmount(500);
    EXPECT_EQ(500,product.getAmount());
  }


TEST(ProducInSupplyTest, ProductItemTestFilled){
    ProductInSupply product = ProductInSupply();
    Product *item = new Product();
    item->setProductAnalyticID(12345);
    item->setProductItemNumber("1");
    item->setProductCategoryName("white cheez");
    item->setProductConditions("conditions");
    item->setProductDescription("brine");
    product.setProductItem(*item);

    EXPECT_EQ("1",product.getProductItem().getProductItemNumber());
    EXPECT_EQ("white cheez",product.getProductItem().getProductCategoryName());
    EXPECT_EQ("conditions",product.getProductItem().getProductConditions());
    EXPECT_EQ("brine",product.getProductItem().getProductDescription());
    EXPECT_EQ(12345,product.getProductItem().getProductAnalyticID());

   // EXPECT_FALSE(product.isEmpty());
}

TEST(ProducInSupplyTest, ProductItemTestEmpty){
    ProductInSupply product = ProductInSupply();
    Product *item = new Product();
    item->setProductAnalyticID(0);
    item->setProductItemNumber("");
    item->setProductCategoryName("");
    item->setProductConditions("");
    item->setProductDescription("");
    product.setProductItem(*item);

    EXPECT_TRUE(item->getProductItemNumber().empty());
    EXPECT_TRUE(item->getProductCategoryName().empty());
    EXPECT_TRUE(item->getProductConditions().empty());
    EXPECT_TRUE(item->getProductDescription().empty());
    EXPECT_EQ(0,item->getProductAnalyticID());

    //EXPECT_TRUE(product.isEmpty());
}


