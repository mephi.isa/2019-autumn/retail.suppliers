#include "gtest/gtest.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\commercialproposal.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\contract.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\product.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productinsupply.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productpull.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\productsinsuppagr.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\suppagrproductset.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supplementaryagreement.h"
#include "E:\Qt_progects\RetailSupp\RetailSuppForTesting\supply.h"

TEST(ContractTest, IndexTest){
    QDate d;
    Contract c = Contract(0,d,d,"");
    c.setContractIndex(89520);
    EXPECT_EQ(89520,c.getContractIndex());
}


TEST(ContractTest, EmployeeIDTest){
    QDate d;
    Contract c = Contract(0,d,d,"");
    c.setContractEmployeeID(156);
    EXPECT_EQ(156,c.getContractEmployeeID());
}


TEST(ContractTest,ContractSupplierTest){
    QDate d;
    Contract c = Contract(0,d,d,"");
    c.setContractSupplier("OAO CheezzyWorld");
    EXPECT_EQ("OAO CheezzyWorld",c.getContractSupplier());
}

TEST(ContractTest,StatusTest){
    QDate d;
    Contract c = Contract(0,d,d,"");
    c.setContractStatus("approved");
    EXPECT_EQ("approved",c.getContractStatus());
}

TEST(ContractTest,StartDateTestFilled){
    QDate d;
    Contract c = Contract(0,d,d,"");
    d = QDate(2019,05,26);
    c.setContractStartDate(d);
    EXPECT_EQ(d,c.getContractStartDate());
}
TEST(ContractTest,DueDateTestFilled){
    QDate d;
    Contract c = Contract(0,d,d,"");
    d = QDate(2019,05,26);
    c.setContractDueDate(d);
    EXPECT_EQ(d,c.getContractDueDate());
}

TEST(ContractTest,SupplyDateTestFilled){
    QDate d;
    Contract c = Contract(0,d,d,"");
    d = QDate(2019,05,26);
    c.setContractSupplyDate(d);
    EXPECT_EQ(d,c.getContractSupplyDate());
}

TEST(ContractTest,StartDateTestEmpty){
    QDate d;
    Contract c = Contract(0,d,d,"");
    c.setContractStartDate(d);
    EXPECT_TRUE(c.getContractStartDate().isNull());
}
TEST(ContractTest,DueDateTestEmpty){
    QDate d;
    Contract c = Contract(0,d,d,"");
    c.setContractDueDate(d);
    EXPECT_TRUE(c.getContractDueDate().isNull());
}

TEST(ContractTest,SupplyDateTestEmpty){
    QDate d;
    Contract c = Contract(0,d,d,"");
    c.setContractSupplyDate(d);
    EXPECT_TRUE(c.getContractSupplyDate().isNull());
}

TEST(ContractTest,AgreementsTestEmpty){
    QDate d;
    Contract c = Contract(0,d,d,"");
    std::list <SupplementaryAgreement> salist;
    c.setContractAgreements(salist);
    EXPECT_TRUE(salist.empty());
}

TEST(ContractTest, AgreementsTestFilled){
    QDate d,s;
    Contract c = Contract(0,d,d,"");
    std::list <SupplementaryAgreement> salist;
    SupplementaryAgreement sa = SupplementaryAgreement();
    std::list<ProductsInSuppAgr> psalist;
    ProductsInSuppAgr psa = ProductsInSuppAgr();
    Product p = Product();

    p.setProductAnalyticID(56);
    p.setProductCategoryName("x");
    p.setProductConditions("conditions");
    p.setProductDescription("description");
    p.setProductItemNumber("67");

    psa.setPsaAmount(8000);
    psa.setPsaPrice(96541.4);
    psa.setPsaProduct(p);

    psalist.push_back(psa);

    d.setDate(2019,12,3);
    sa.setSuppAgrDueDate(d);
    s.setDate(2019,10,8);
    sa.setSuppAgrSignDate(s);
    sa.setSuppAgrProducts(psalist);
    sa.setSuppAgrStatus("approved");
    salist.push_back(sa);
    c.setContractAgreements(salist);
   /*вот здесь написать все проверки*/

   EXPECT_EQ(d,c.getContractAgreements().back().getSuppAgrDueDate());
   EXPECT_EQ(s,c.getContractAgreements().back().getSuppAgrSignDate());
   EXPECT_EQ("approved",c.getContractAgreements().back().getSuppAgrStatus());
   EXPECT_EQ(8000,c.getContractAgreements().back().getSuppAgrProducts().back().getPsaAmount());
   EXPECT_EQ(96541.4,c.getContractAgreements().back().getSuppAgrProducts().back().getPsaPrice());
   EXPECT_EQ(56,c.getContractAgreements().back().getSuppAgrProducts().back().getPsaProduct().getProductAnalyticID());
EXPECT_EQ("x",c.getContractAgreements().back().getSuppAgrProducts().back().getPsaProduct().getProductCategoryName());
EXPECT_EQ("conditions",c.getContractAgreements().back().getSuppAgrProducts().back().getPsaProduct().getProductConditions());
EXPECT_EQ("description",c.getContractAgreements().back().getSuppAgrProducts().back().getPsaProduct().getProductDescription());
EXPECT_EQ("67",c.getContractAgreements().back().getSuppAgrProducts().back().getPsaProduct().getProductItemNumber());


}


TEST(ContractTest,ProposalTestFilled){
    QDate d,s,st;
    Contract c = Contract(0,d,d,"");
    CommercialProposal prop;
    ProductPull pull;
    std::list <ProductPull> prp;
    Product p;

    p.setProductAnalyticID(56);
    p.setProductCategoryName("x");
    p.setProductConditions("conditions");
    p.setProductDescription("description");
    p.setProductItemNumber("67");

    d.setDate(2019,12,3);
    s.setDate(2019,10,8);
    st.setDate(2019,10,20);

    pull.setPullItem(p);
    pull.setPullItemAmount(500);
    pull.setPullItemPrice(300.78);

    prp.push_back(pull);

    prop.setProposalEmployeeID(234);
    prop.setProposalIndex(44);
    prop.setProposalDueDate(d);
    prop.setProposalSignDate(s);
    prop.setProposalStartDate(st);
    prop.setProposalStatus("waiting");
    prop.setProposalSupplier("OAO Chezzy world");
    prop.setProposalProducts(prp);
    c.setContractProposal(prop);

EXPECT_EQ(d,c.getContractProposal().getProposalDueDate());
EXPECT_EQ(s,c.getContractProposal().getProposalSignDate());
EXPECT_EQ(st,c.getContractProposal().getProposalStartDate());
EXPECT_EQ(234,c.getContractProposal().getProposalEmployeeID());
EXPECT_EQ(44,c.getContractProposal().getProposalIndex());
EXPECT_EQ("waiting",c.getContractProposal().getProposalStatus());
EXPECT_EQ("OAO Chezzy world",c.getContractProposal().getProposalSupplier());
EXPECT_EQ(56,c.getContractProposal().getProposalProducts().back().getPullItem().getProductAnalyticID());
EXPECT_EQ("x",c.getContractProposal().getProposalProducts().back().getPullItem().getProductCategoryName());
EXPECT_EQ("conditions",c.getContractProposal().getProposalProducts().back().getPullItem().getProductConditions());
EXPECT_EQ("description",c.getContractProposal().getProposalProducts().back().getPullItem().getProductDescription());
EXPECT_EQ("67",c.getContractProposal().getProposalProducts().back().getPullItem().getProductItemNumber());

EXPECT_EQ(500,c.getContractProposal().getProposalProducts().back().getPullItemAmount());
EXPECT_EQ(300.78,c.getContractProposal().getProposalProducts().back().getPullItemPrice());


}

TEST(ContractTest,ProductsTestEmpty){
    QDate d;
    Contract c = Contract(0,d,d,"");
    std::list <ProductPull> prp;
    c.setContractProducts(prp);
    EXPECT_TRUE(c.getContractProducts().empty());
}


TEST(ContractTest,ProductsTestFilled){
    QDate d;
    Contract c = Contract(0,d,d,"");
    ProductPull pull;
    std::list <ProductPull> prp;
    Product p;

    p.setProductAnalyticID(56);
    p.setProductCategoryName("x");
    p.setProductConditions("conditions");
    p.setProductDescription("description");
    p.setProductItemNumber("67");

    pull.setPullItem(p);
    pull.setPullItemAmount(500);
    pull.setPullItemPrice(300.78);

    prp.push_back(pull);

    c.setContractProducts(prp);

    EXPECT_EQ(56,c.getContractProducts().back().getPullItem().getProductAnalyticID());
    EXPECT_EQ("x",c.getContractProducts().back().getPullItem().getProductCategoryName());
    EXPECT_EQ("conditions",c.getContractProducts().back().getPullItem().getProductConditions());
    EXPECT_EQ("description",c.getContractProducts().back().getPullItem().getProductDescription());
    EXPECT_EQ("67",c.getContractProducts().back().getPullItem().getProductItemNumber());
    EXPECT_EQ(500,c.getContractProducts().back().getPullItemAmount());
    EXPECT_EQ(300.78,c.getContractProducts().back().getPullItemPrice());

}



